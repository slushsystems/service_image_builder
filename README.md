# Service Image Builder

This is a Slush Systems tool for building service images. Slush Systems
creates continuously updated and deployed infrastructure and tooling
container images. This tool creates, tags, and pushes those images the
registry.

SIB is configured through environment variables:

- CI_COMMIT_SHA — Automatically provided by gitlab CI; commit sha from source
    control; # TODO pick this up from VCS directly if unset
- SIB_BUILD_DIR — Docker build context directory; optional & defaults to $PWD
- SIB_DOCKER_FILE — Dockerfile to use in build; optional & defaults to
    'Dockerfile'
- SIB_IMAGE_REPO — *required* Docker repo
- SIB_REGISTRY_USER — *required* Username to authenticate with the registry
- SIB_REGISTRY_PASS — *required* Password to authenticate with the registry
- SIB_UPSTREAM_TAG — Upstream tag to compare against and push to; optional &
    defaults to 'latest'
