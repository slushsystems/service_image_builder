# frozen_string_literal: true

RSpec.describe SIB do
  it 'has a version number' do
    expect(SIB::VERSION).not_to be nil
  end
end
