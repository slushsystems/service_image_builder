#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/service_image_builder'

# This is a sha256 sum of the ouput of `dnf list installed` which contains a
# list of installed packages and their associated versions. A change in this
# value from one build to the next indicates that one or more packages have
# changed.
#
# A change in package_manifest should trigger a new tag
#
# images pulled from upstream will have this in a docker label
# otherwise, it needs to
#

build_dir = ENV.fetch('SIB_BUILD_DIR', '.')
current_commit_id = ENV.fetch('CI_COMMIT_SHA')
dockerfile = ENV.fetch('SIB_DOCKER_FILE', 'Dockerfile')
image_repo = ENV.fetch('SIB_IMAGE_REPO')
image_upstream_tag = ENV.fetch('SIB_UPSTREAM_TAG', 'latest')
registry_pass = ENV.fetch('SIB_REGISTRY_PASS')
registry_user = ENV.fetch('SIB_REGISTRY_USER')
push = ENV.fetch('SIB_PUSH', true)
squash = ENV.fetch('SIB_SQUASH', false)

# Convert boolean vars
# TODO put this in a configuration class
def bool!(input)
  return input if input.is_a?(TrueClass) || input.is_a?(FalseClass)
  return true if input == 'true'
  return false if input == 'false'

  raise 'Invalid boolean input'
end

push = bool!(push)
squash = bool!(squash)

Docker.authenticate!('username' => registry_user, 'password' => registry_pass)

upstream = SIB::Image.import(repo: image_repo, tag: image_upstream_tag)

build_args = {
  repo: image_repo,
  build_dir: build_dir,
  dockerfile: dockerfile,
  labels: {
    'maintainer' => 'Slush Systems <ci@slush.systems>',
    'systems.slush.commit-id' => current_commit_id,
    'systems.slush.timestamp' => Time.now.utc.iso8601
  },
  squash: squash
}

canary = SIB::Image.build(build_args)

if push && (upstream.nil? || (canary <=> upstream).positive?)
  canary.push_tag(tag: image_upstream_tag)
  canary.push_tag(tag: canary.factor_hash)
end
