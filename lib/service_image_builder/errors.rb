# frozen_string_literal: true

module SIB
  # Error is SIB's top level error
  class Error < StandardError
    def initialize(msg)
      SIB.log.error(msg)
      super
    end
  end
end
