# frozen_string_literal: true

# SIB is a module for building Slush Systems docker images
module SIB
  # SIB::log serves as a module-level logging method
  def self.log
    @log ||= Logger.new($stdout).tap do |lager|
      lager.formatter = proc do |severity, datetime, progname, msg|
        out = msg.is_a?(String) ? msg : msg.ai(multiline: false)
        "#{datetime} #{severity} #{progname}: #{out}\n"
      end
    end
  end
end
